<!DOCTYPE html>
<?php
define('BOOK', true);
require "include/common.php";
$q = "SELECT * FROM `web`";
$web=$conn->query($q);
$webrow=$web->fetch_array();
$l=$q = "SELECT * FROM `book`";
$book=$conn->query($l);
$bookrow=$book->fetch_array();
?>
<html class="loaded">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

    <title>《<?php echo $bookrow['shuming']; ?>》_<?php echo $bookrow['zuozhe']; ?>著_<?php echo $webrow['webname']; ?></title>
    <meta name="description" content="<?php echo $webrow['descrption']; ?>">
    <meta name="keywords" content="<?php echo $webrow['keywords']; ?>">
    <meta name="robots" content="all">
    <meta name="googlebot" content="all">
    <meta name="baiduspider" content="all">

    <meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=1">
    <meta name="renderer" content="webkit">
    <script async="" src="./images/qidian/yep.min.js"></script>
    <script src="./images/qidian/stats.js" name="MTAH5" sid="500451537"></script>
    <script src="./images/qidian/push.js"></script>
    <link rel="stylesheet" data-ignore="true" href="./images/qidian/emoji.c4e81.css">
    <link data-ignore="true" rel="stylesheet" href="./images/qidian/module.a841b.css">
    <link data-ignore="true" rel="stylesheet" href="./images/qidian/layout.5de0f.css">
    <link data-ignore="true" rel="stylesheet" href="./images/qidian/book_details.1baf2.css">
    <link charset="utf-8" rel="stylesheet" href="./images/qidian/Panel.css">
    <link charset="utf-8" rel="stylesheet" href="./images/qidian/LightTip.css">
    <link charset="utf-8" rel="stylesheet" href="./images/qidian/Popup.css">
    <link charset="utf-8" rel="stylesheet" href="./images/qidian/Drag.css">
    <style name="lbf-overlay">.lbf-overlay {
            position: fixed;
            top: 0;
            left: 0;
        }</style>
</head>
<body style="zoom: 1;">

<div class="wrap">
    <div id="pin-nav" class="pin-nav-wrap need-search" data-l1="40">
        <div class="box-center cf">
            <div class="nav-list site-nav fl">
                <ul>
                    <li class="site"><a class="pin-logo" href="" data-eid="qd_A43"><span></span></a></li> //网站logo
                    <?php
                    $navi="SELECT * FROM `navigate` ORDER BY `order` ASC ";
                    $nvrs=$conn->query($navi);
                    while ($navirow=$nvrs->fetch_array())
                    {
                    ?>
                    <li><a href="<?php echo $navirow['link']; ?>" target="_blank"><?php echo $navirow['lanmu']; ?></a></li>
                    <?php } ?>


                </ul>
            </div>
            <div class="nav-list min-user fr">
                <ul>

                    <li class="line"></li>
                    <li class="sign-out">
                        <a id="pin-login" href="login.php" data-eid="qd_A64">登录</a>
                        <a class="reg" href="" target="_blank">注册</a>
                    </li>


                </ul>
            </div>
        </div>
    </div>


    <div id="j-topHeadBox" class="top-bg-op-box " data-cookie="1" style="background-image:url(<?php echo $webrow['bgimg']; ?>)"  data-l1="1">
        <a class="jumpWrap"  href="">
            <span class="op-tag"></span>
        </a>
    </div>
    <div class="crumbs-nav center990  top-op" data-l1="1">
    </div>
    <div class="border-shadow ">
        <span></span>
        <span></span>
    </div>
    <div class="book-detail-wrap center990">
        <div class="book-information cf" data-l1="2">
            <div class="book-img">
                <a class="J-getJumpUrl" id="bookImg"  href="">
                    <img src="<?php echo $bookrow['bookimg']; ?>"></a>
            </div>

            <div class="book-info ">
                <h1>

                    <em><?php echo $bookrow['shuming']; ?></em>
                    <span><a class="writer" target="_blank" ><?php echo $bookrow['zuozhe']; ?></a> 著</span>

                </h1>
                <p class="tag"><span class="blue">连载</span>
                    <span class="blue">签约</span>
                    <span class="blue">VIP</span>
                    <a>都市</a>
                    <a>都市生活</a>
                </p>

                <p class="intro"><?php echo $bookrow['jianjie']; ?></p>
                <p><em>
                        <style>

                            .HTWFyvRh {
                                font-family: 'HTWFyvRh' !important;
                                display: initial !important;
                                color: inherit !important;
                                vertical-align: initial !important;
                            }</style>
                        <span class="HTWFyvRh">12</span></em><cite>万字</cite><i>|</i>

                    <em>
                        <style>
                            .HTWFyvRh {
                                font-family: 'HTWFyvRh' !important;
                                display: initial !important;
                                color: inherit !important;
                                vertical-align: initial !important;
                            }</style>
                        <span class="HTWFyvRh">25</span></em><cite>万总推荐</cite><i>|</i><em>
                        <style>

                            .HTWFyvRh {
                                font-family: 'HTWFyvRh' !important;
                                display: initial !important;
                                color: inherit !important;
                                vertical-align: initial !important;
                            }</style>
                        <span class="HTWFyvRh">8</span></em><cite>周推荐</cite></p>

                <p> <a class="red-btn J-getJumpUrl ">欢迎阅读</a>
                    <a class="blue-btn add-book">加入收藏</a>
                    <a class="blue-btn">纯手打字</a>

                    <a class="blue-btn" id="downloadAppBtn"  style="line-height: 26px;">写到完本</a>
                </p>

            </div>

            <div class="comment-wrap">
                <div id="commentWrap">

                    <div class="j_getData hidden" style="display: block;">
                        <h4 id="j_bookScore"><span><cite id="score1">8</cite><em>.</em></span><i id="score2">6</i></h4>
                        <p id="j_userCount"><span>170</span>次阅读</p>
                    </div>
                </div>

            </div>
            <div class="take-wrap">
                <a class="blue" id="subscribe" href="javascript:" data-eid="qd_G13"><em class="iconfont"></em>订阅</a>
            </div>
        </div>


        <div class="content-nav-wrap cf" data-l1="3">
            <div class="nav-wrap fl">
                <ul>

                    <li class="j_catalog_block act">
                        <a class="lang" href="javascript:" id="j_catalogPage" data-eid="qd_G16">目录<i><span id="J-catalogCount">(434章)</span></i></a></li>

                </ul>
            </div>
        </div>
        <div class="catalog-content-wrap" id="j-catalogWrap" data-l1="14" style="display: block;">
            <div class="volume-wrap">
                <?php
                $fjsql = "SELECT * FROM `fenjuan` ORDER BY `id` ASC ;";
                $fjrs = $conn->query($fjsql);
                while ($fjrow = $fjrs->fetch_array()) {
                    $wzsql = "SELECT `aid`,`title` FROM `wenzhang` WHERE `fjid`= '$fjrow[id]' ORDER BY `aid` ASC ;";
                    $wzrs = $conn->query($wzsql);
                    $nu = mysqli_num_rows($wzrs);

                    ?>
                    <div class="volume">
                        <div class="cover"></div>
                        <h3>
                            <a class="subscri"><em class="btn"><b class="iconfont"></b>分卷阅读</em></a>
                            <?php echo $fjrow['name'] ?><i>·</i>共<?php echo $nu; ?>章<span class="free"> 免费</span><em
                                    class="count"></em></h3>
                        <ul class="cf">
                            <?php
                            $i = 1;
                            while ($wzrow = $wzrs->fetch_array()) {
                                ?>
                                <li data-rid="<?php echo $i; ?>"><a href="content.php?aid=<?php echo $wzrow['aid']; ?>" target="_blank" title="<?php echo $wzrow['title']; ?>"><?php echo $wzrow['title']; ?></a>

                                </li>
                                <?php
                                $i++;
                            }
                            ?>

                        </ul>
                    </div>
                    <?php
                }
                ?>

            </div>

        </div>


    </div>
    <div class="footer">
        <div class="box-center cf">
            <div class="footer-text">
                <p>提示：<?php echo $webrow['tip'] ;?></p>
            </div>
            <div class="friend-link">
                <em>友情链接：</cite></em>
                <?php
                $frsql="SELECT * FROM `friendlink` ORDER BY `paixu` ASC ";
                $frresult=$conn->query($frsql);
                while ($frrow=$frresult->fetch_array()){
                ?>
                <a href="<?php  echo $frrow['frurl']; ?>" target="_blank"><?php  echo $frrow['website']; ?></a>
                <?php  } ?>
            </div>
            <div class="footer-menu dib-wrap">
                <a href="http://www.liulei.com.cn" target="_blank">关于作者</a>
                <a href="mailto:247483085@qq.com" target="_blank">联系我们</a>
                <a href="" target="_blank">加入我们</a>
                <a href="" target="_blank">帮助中心</a>
                <a href="" target="_blank">举报中心</a>
            </div>
            <div class="copy-right">
                <?php echo $webrow['foot'];?>
            </div>

        </div>
    </div>

</div>

</body>
</html>