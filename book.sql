-- phpMyAdmin SQL Dump
-- version phpStudy 2014
-- http://www.phpmyadmin.net
--
-- 主机: localhost
-- 生成日期: 2020 年 02 月 01 日 12:04
-- 服务器版本: 5.5.53
-- PHP 版本: 5.2.17

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- 数据库: `book`
--

-- --------------------------------------------------------

--
-- 表的结构 `admin`
--

CREATE TABLE IF NOT EXISTS `admin` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` text,
  `password` text,
  `shenfen` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- 转存表中的数据 `admin`
--

INSERT INTO `admin` (`id`, `username`, `password`, `shenfen`) VALUES
(1, 'admin', 'e10adc3949ba59abbe56e057f20f883e', 1);

-- --------------------------------------------------------

--
-- 表的结构 `book`
--

CREATE TABLE IF NOT EXISTS `book` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `shuming` text COMMENT '书名',
  `zuozhe` text COMMENT '作者',
  `jianjie` text COMMENT '简介',
  `zishu` text NOT NULL COMMENT '字数',
  `bookimg` text,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- 转存表中的数据 `book`
--

INSERT INTO `book` (`id`, `shuming`, `zuozhe`, `jianjie`, `zishu`, `bookimg`) VALUES
(1, '这里是书籍名称', '作者', '这里是书籍名称这里是书籍名称这里是书籍名称这里是书籍名称这里是书籍名称这里是书籍名称这里是书籍名称', '112', 'files/9ffe5b9001509cba583033fe3c38ca1d.jpg');

-- --------------------------------------------------------

--
-- 表的结构 `fenjuan`
--

CREATE TABLE IF NOT EXISTS `fenjuan` (
  `name` text,
  `order` int(11) DEFAULT NULL,
  `id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- 转存表中的数据 `fenjuan`
--

INSERT INTO `fenjuan` (`name`, `order`, `id`) VALUES
('第一卷  生长在乡镇', 1, 2);

-- --------------------------------------------------------

--
-- 表的结构 `friendlink`
--

CREATE TABLE IF NOT EXISTS `friendlink` (
  `frid` int(11) NOT NULL AUTO_INCREMENT,
  `website` text COMMENT '网站名称',
  `frurl` text COMMENT '网站链接',
  `paixu` int(11) DEFAULT NULL COMMENT '排序',
  PRIMARY KEY (`frid`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- 转存表中的数据 `friendlink`
--

INSERT INTO `friendlink` (`frid`, `website`, `frurl`, `paixu`) VALUES
(2, '刘磊博客', 'http://www.liulei.com.cn', 2);

-- --------------------------------------------------------

--
-- 表的结构 `navigate`
--

CREATE TABLE IF NOT EXISTS `navigate` (
  `nid` int(11) NOT NULL AUTO_INCREMENT,
  `lanmu` text COMMENT '栏目',
  `link` text COMMENT '链接',
  `order` int(11) DEFAULT NULL COMMENT '排序',
  PRIMARY KEY (`nid`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=5 ;

--
-- 转存表中的数据 `navigate`
--

INSERT INTO `navigate` (`nid`, `lanmu`, `link`, `order`) VALUES
(4, '刘磊博客', 'http://www.liulei.com.cn', 2);

-- --------------------------------------------------------

--
-- 表的结构 `web`
--

CREATE TABLE IF NOT EXISTS `web` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `webname` text COMMENT '网站名称',
  `keywords` text COMMENT '网站关键词',
  `descrption` text COMMENT '网站描述',
  `foot` text COMMENT '底部代码',
  `tip` text COMMENT '上网提示',
  `bgimg` text COMMENT '背景图片',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- 转存表中的数据 `web`
--

INSERT INTO `web` (`id`, `webname`, `keywords`, `descrption`, `foot`, `tip`, `bgimg`) VALUES
(1, 'webname', 'keywords', 'description', '<p><span>Copyright © 2002-2020 www.qidian.com All Rights Reserved</span>版权所有 上海玄霆娱乐信息科技有限公司</p>\r\n                <p><a target="_blank"\r\n                      href="http://www.beian.gov.cn/portal/registerSystemInfo?recordcode=31011502001275"><img\r\n                                src="./images/qidian/o5lajshfcw.png"><span>沪公网安备 31011502001275号</span></a>网站备案/许可证号：<a\r\n                            href="http://beian.miit.gov.cn/" target="_blank">沪B2-20080046-1</a>&nbsp;出版经营许可证：新出发沪批字第\r\n                    U3718 号&nbsp;网络文化经营许可证：沪网文[2015]0081-031\r\n                    新出网证（沪）字 010</p>\r\n                <p>请所有作者发布作品时务必遵守国家互联网信息管理办法规定，我们拒绝任何色情小说，一经发现，即作删除！举报电话：010-59357051</p>\r\n                <p>本站所收录的作品、社区话题、用户评论、用户上传内容或图片等均属用户个人行为。如前述内容侵害您的权益，欢迎举报投诉，一经核实，立即删除，本站不承担任何责任</p>\r\n                <p>联系方式 总机 021-61870500 地址：中国（上海）自由贸易试验区碧波路690号6号楼101、201室</p>', 'tip', 'files/f1f0804df363b0ed3f9bb770bf4d1d1d.jpg');

-- --------------------------------------------------------

--
-- 表的结构 `wenzhang`
--

CREATE TABLE IF NOT EXISTS `wenzhang` (
  `aid` int(11) NOT NULL AUTO_INCREMENT,
  `fjid` int(11) NOT NULL,
  `title` text NOT NULL,
  `content` text NOT NULL,
  `time` date NOT NULL,
  PRIMARY KEY (`aid`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- 转存表中的数据 `wenzhang`
--

INSERT INTO `wenzhang` (`aid`, `fjid`, `title`, `content`, `time`) VALUES
(1, 2, '第一章  又在复读', '<p>&nbsp; &nbsp; &nbsp; &nbsp;刘国庆很郁闷，这都复读第二年了，还是没有考上本科，早知道去年走个大专，高中毕业 的同学上大专的 都大二马上毕业了，他还在这个复读班拼命的复读，最关键的是，成绩还不怎么地好。明年能不能考上本科还是未知数，这样复习下去动力都没有了，万一今年走不上本科，又是丢人现眼的事情了。平时周末连老家都不愿意回，就是怕左邻右舍的七大姑八大姨来问候，丢人现眼啊。头都抬不起来。</p><p>&nbsp; &nbsp; &nbsp; 这次周末回家再家里也是闲着没事，弹弓打鸟是刘国庆最喜欢干的事情，初中时候，舅舅家还有压杆式的气枪，偷偷扛出来到湖边田野打鸟是他最喜欢干的事情。这个周末要是能和考上大学的那些好友同学一起出去玩耍一下那是多么好的事情。这现在这个样子在家里，无聊的一塌糊涂，书也看不进去，学似乎也学不好了，快成为老油条了。<br/></p>', '2020-01-29');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
